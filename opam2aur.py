#!/usr/bin/env python3

import argparse
import pprint
import os
import subprocess
import re
import sys
from pathlib import Path
import getpass
import socket

from lark.lark import Lark
from lark.visitors import Transformer
import aur
from jinja2 import Template

class CatchableError(Exception):
    pass

def normalize_version(version):
    '''
    takes a string of type PACKAGENAME.0.1.2 and returns [0,1,2]
    '''
    version = filter(lambda x: len(x) > 0, re.sub(r'[^0-9.]','', version).split('.'))
    return [int(x) for x in version]

def strip_tree(tree):
    if hasattr(tree, 'strip'):
        import pdb; pdb.set_trace()
        return tree.strip()
    else:
        return [strip_tree(x) for x in tree.children]


class OpamTransformer(Transformer):
    def __init__(self, context):
        self.context = context

    def start(self, items):
        return dict(items)

    def item(self, items):
        return (items[0], items[1])

    def key(self, item):
        return item[0].strip()

    def value(self, item):
        return item[0]

    def array(self, items):
        return list(items)

    def string(self, s):
        (s,) = s
        return bytes(s[1:-1], 'utf-8').decode('unicode-escape')

    def options(self, items):
        return set(items)

def compose_instructions(ins):
    if len(ins) == 0:
        return ''
    if type(ins) == str:
        return ins
    if type(ins[0]) == list:
        return '\n  '.join(' '.join(x) for x in ins)
    else:
        return ' '.join(ins)

def get_arch_repo(ocaml_dep):
    base = {
        'ocamlfind': 'ocaml-findlib',
        'ocamlbuild': None, # use same name as key
        'ctypes': 'ocaml-ctypes',
        'base-bytes': 'ocaml',
        'base-unix': 'ocaml',
        'camlp4': None,
        'yojson': 'ocaml-yojson',
        'ulex': None,

    }
    if ocaml_dep in base:
        return base[ocaml_dep] or ocaml_dep
    stripped_name = re.sub(r'^ocaml', '', ocaml_dep)
    expected_name = 'ocaml-{}'.format(stripped_name)
    aur_pkgs = aur.search(expected_name)
    if expected_name not in [re.sub('-(git|bin)', '', x.name) for x in aur_pkgs]:
        raise CatchableError('Could not find OPAM package {}'.format(ocaml_dep))
    return expected_name


def get_deps(depends, build=False):
    def _deps():
        for dep in depends:
            if type(dep) == str:
                yield get_arch_repo(dep.strip())
            elif len(dep.children) == 1 or (len(dep.children) > 1 and build and 'build' in dep.children[1]):
                yield get_arch_repo(dep.children[0].strip())
    return ' '.join(["'{}'".format(x) for x in set(list(_deps()) + [] if build else ['ocaml'])])

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('package', help='Package to convert')
    parser.add_argument('--version', help='Version of the package to convert', default=None)
    parser.add_argument('--maintainer', help='Maintainer of the AUR package')
    args = parser.parse_args()

    context = {'name': args.package}
    package_path = os.path.join(Path.home(), '.opam/repo/default/packages/', args.package)


    version = args.version or None
    if version is None:
        for dirname, dirnames, _ in os.walk(package_path):
            package_version_dir = max(dirnames, key=normalize_version)
            version = '.'.join(map(str, normalize_version(package_version_dir)))
            break
    else:
        package_version_dir = '{}.{}'.format(args.package, version)
    context['version'] = version

    version_path = os.path.join(package_path, package_version_dir)
    descr_path = os.path.join(version_path, 'descr')
    opam_path = os.path.join(version_path, 'opam')
    url_path = os.path.join(version_path, 'url')
    with open(descr_path, 'r') as f:
        context['description'] = f.read().split('\n')[0].strip() # Get first line of description, stripped

    print('Latest version:', version)
    print('OPAM path:', opam_path)

    with open('opam.lark', 'r') as f:
        opam_parser = Lark(f.read(), transformer=OpamTransformer(context), parser='lalr')

    with open(opam_path, 'r') as f:
        opam = f.read()

    opam = opam_parser.parse(opam)
    pprint.pprint(opam)
    context['license'] = opam.get('license', 'unknown')
    user = getpass.getuser()
    context['maintainer'] = args.maintainer or '{} <{}@{}>'.format(user, user, socket.gethostname())
    context['build'] = compose_instructions(opam['build'])
    context['install'] = compose_instructions(opam['install'])
    context['depends'] = get_deps(opam['depends'])
    context['makedepends'] = get_deps(opam['depends'], build=True)

    with open(url_path, 'r') as f:
        url = f.read()
    url = opam_parser.parse(url)
    context['source'] = url.get('archive', url.get('http'))
    context['md5sum'] = url['checksum']
    
    pprint.pprint(context)
    with open('PKGBUILD.template') as f:
        t = Template(f.read())
    with open('PKGBUILD', 'w') as f:
        f.write(t.render(context))

    subprocess.Popen(['git', 'clone', 'ssh://aur@aur.archlinux.org/ocaml-{}.git'.format(args.package)])
    subprocess.Popen('cd ocaml-{} && cp ../PKGBUILD . && makepkg --printsrcinfo > .SRCINFO && git add PKGBUILD .SRCINFO && git commit -m "Update package"'.format(args.package), shell=True) # nicely injectible

if __name__ == '__main__':
    try:
        main()
    except CatchableError as e:
        print('ERROR:', e)
